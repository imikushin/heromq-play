scalaVersion := "2.10.2"

libraryDependencies ++= Seq(
  "org.clapper" %% "grizzled-slf4j" % "1.0.1",
  "org.scala-lang" % "scalap" % "2.10.2"
)
